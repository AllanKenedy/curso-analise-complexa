\section{Funções Analíticas}
\label{sc:funcoes-analiticas}

Seja $\K$ um corpo.
Um \emph{valor absoluto} em $\K$ é uma aplicação $\norm{\cdot} \from \K \to [0, \infty[$ com valores reais não-negativos tal que
\begin{itemize}
  \item (Hausdorff) $\norm{x} = 0 \text{ se, e somente se, } x = 0$,
  \item (Multiplicatividade) $\norm{xy} = \norm{x}\norm{y}$, e
  \item (Desigualdade Triangular) $\norm{x + y} \leq \norm{x} + \norm{y}$.
\end{itemize}
Por exemplo, $\K = \mathbb{R}$ ou $\mathbb{C}$.
Todo tal valor absoluto $\norm{\cdot}$ induz uma métrica $\d$ por $\d(x, y) := \norm{x - y}$.

\begin{defn}
  Uma \emph{série} $\sum_n a_n = a_0 + a_1 + a_2 + \cdots$ com $a_0$, $a_1$, \ldots em $\K$ \emph{converge} se a sequência $(s_n)$ dos seus truncamentos $s_n = a_1 + \cdots + a_n$ converge (em $\K$).
  Ela \emph{converge absolutamente} se $\sum_{n} \norm{a_n}$ converge (em $[0, \infty[$).
\end{defn}

\begin{prop}
  Seja $\sum_{n} a_n$ uma série.
  Se ela converge absolutamente e $\K$ é completo, então converge.
\end{prop}
\begin{proof}
  Como $\K$ é completo, dada uma série $\sum_{n} u_n$, observemos q a sequência $(s_n)$ dos seus truncamentos finitos $s_n = u_1 + \cdots + u_n$ converge se, e tão-somente se, $s_{n, \ldots, m} = u_n + \cdots + u_m \to 0$ para $n \to \infty$;
  isto é, $(s_n)$ converge se, e tão-somente se, para todo $\epsilon > 0$ existe $N$ tal que $\norm{s_{n, \ldots, m}} < \epsilon$ para todo $n, m \geq N$.%Uma notação mais conveniente seria |s_{n-1}-s_m|<\epsilon, onde s_{n-1}-s_m = u_n + \cdots + u_m, pois poderia argumetar que a sequência (s_n) é de Cauchy e como K é completo, (s_n) é convergente

  Pela desigualdade triangular, $\norm{a_n + \cdots + a_m} \leq \norm{a_n} + \cdots + \norm{a_m}$.
  Logo, se $\norm{a_n} + \cdots + \norm{a_m} \to 0$, então $\norm{a_n + \cdots + a_m} \to 0$.
  Logo, pela observação, a sequência dos truncamentos finitos $s_n = a_1 + \cdots + a_n$ converge;
  isto é, $\sum_{n} a_n$ converge.
\end{proof}

Para $(a_n)$ uma sequência em $\K$, denote
\[
  \liminf a_n \coloneqq \lim_{n \to \infty} \inf \{ a_n : n \in \mathbb{N} \}
  \quad \text{ e } \quad
  \limsup a_n \coloneqq\lim_{n \to \infty} \sup \{ a_n : n \in \mathbb{N} \};
\]
ou, alternativamente,
\[
  \underlim a_n \coloneqq \liminf a_n
  \quad \text{ e } \quad
  \overlim a_n \coloneqq \limsup a_n.
\]
Os limites são monotonamente crescentes respectivamente decrescentes; por isso sempre existem (se incluímos a possibilidade dos limites $\pm \infty$).%Sugestão: Seria bom incluir um apêndice sobre limites superior e inferior; ou deixar alguma referência para o estudo do assunto, como por exemplo 'Curso de análise v.1', de Elon Lages Lima

Uma \emph{série de potências em torno de $a$} em $\K$ é uma série infinita da forma
\[
  \sum_{n \in \mathbb{N}} a_n (z-a)^n.
\]

  \subsection{Raio de convergência}
  \label{ssc:raio-de-convergencia}

  A \emph{série geométrica} é a série de potências (em torno de $0$)
  \[
    \sum_{n \in \mathbb{N}} z^n.
  \]
  Pela \emph{soma telescópica},
  \[
    (1 - z)(1 + z + \cdots + z^n)
    =
    1 - z^{n+1}
  \]
  Logo, se $\norm{z} < 1$, então
  \[
    \sum_{n} z^n
    =
    \frac{1}{1 - z},
  \]
  e se $\norm{z} > 1$, então $\sum_{n} z^n$ diverge.

  \begin{thm}
    Para uma série de potências $\sum_{n} a_n (z-a)^n$ define o seu \emph{raio de convergência} $R$ por
    \[
      \frac{1}{R}
      \coloneqq
      \limsup \norm{a_n}^{\frac{1}{n}}
      \quad \text{ em } [0, \infty].
    \]
    \begin{enumerate}
      \item Se $\norm{z - a} < R$, então a série converge absolutamente.
      \item Se $\norm{z - a} > R$, então a série diverge.
      \item Se $r < R$, então a série converge uniformemente na bola $\{ z : \norm{z} \leq r \}$.
    \end{enumerate}
  \end{thm}
  \begin{proof}
    Pelo traslado $z \mapsto z - a$, podemos supor $a = 0$.
    \begin{enumerate}
      \item
        Temos
        \[
          \limsup \norm{a_n}^{\frac{1}{n}}
          \leq
          \frac{1}{R}
          \quad \text{ em } [0, \infty].
        \]
        se, e tão-somente se, existe $N$ tal que $\norm{a_n}^{\frac{1}{n}} \leq \frac{1}{R}$ para todo $n \geq N$.
        Se $\norm{z} < R$, então existe $r < R$ com $\norm{z} \leq r$.
        Logo, para $n \geq N$,
        \[
          \norm{a_n z^n}
          \norm{a_n} \norm{z^n}
          \leq
          (\frac{r}{R})^n.
        \]
        Como $\frac{r}{R} < 1$, a série $\sum_{n \geq N} a_n z^n$ é dominada pela série geométrica;
        logo converge absolutamente em cada ponto, e uniformemente pelo Teste de Weierstrass \cref{thm:teste-weierstrass} .

        Se $\norm{z} > R$, então existe $r > R$ com $\norm{z} \geq r$.
        Temos
        \[
          \limsup \norm{a_n}^{\frac{1}{n}}
          \geq
          \frac{1}{R}
          \quad \text{ em } [0, \infty].
        \]
        se, e tão-somente se, existem $n_0$, $n_1$, \ldots em $\mathbb{N}$ tal que $\norm{a_{n_0}}^{\frac{1}{n_0}}$, $\norm{a_{n_1}}^{\frac{1}{n_1}}$ \ldots $ \geq \frac{1}{R}$.
        Logo a série $\sum_{n} a_n z^n$ tem uma infinitude de adendos com índices $n = n_0$, $n_1$, \ldots com
        \[
          \norm{a_n z^n} \geq \frac{r}{R}.
        \]
        Como $(\frac{r}{R})^n \to \infty$, estes adendos são ilimitados;
        logo $\sum_{n} a_n z^n$ não converge.
        \qedhere
    \end{enumerate}
  \end{proof}

  \begin{prop}
    \label{prop:raio-convergencia}
    Seja $\sum_{n} a_n (z-a)^n$ uma série de potências cujo \emph{raio de convergência} $R$ é definido por
    \[
      \frac{1}{R}
      \coloneqq
      \limsup \norm{a_n}^{\frac{1}{n}}
      \quad \text{ em } [0, \infty].
    \]
    Se
    \[
      \left(\norm*{\frac{a_n}{a_{n+1}}}\right)
    \]
    converge, então
    \[
      \norm*{\frac{a_n}{a_{n+1}}} \to R.
    \]
  \end{prop}
  \begin{proof}
    Exista $\alpha = \lim \norm{\frac{a_n}{a_{n+1}}}$.
    Se $r < \alpha$, então existe $N$ tal que $r < \norm{\frac{a_n}{a_{n+1}}}$ para todo $n \geq N$.

    Suponhamos $a = 0$.

    Se $\norm{z} = s < r$, então estimemos, para $n \geq N$, usando o \emph{produto telescópico} $\frac{\norm{a_n}}{\norm{a_N}} = \frac{\norm{a_n}}{\norm{a_{n-1}}}
      \cdots
      \frac{\norm{a_{N+1}}}{\norm{a_N}}
      $ que
    \[
      \norm{a_n z^n}
      =
      \norm{a_n}
      \norm{z^n}
      =
      \frac{\norm{a_n}}{\norm{a_{n-1}}}
      \cdots
      \frac{\norm{a_{N+1}}}{\norm{a_N}}
      \norm{a_N}
      \norm{z^n}
      <
      \norm{a_N}
      \xi^n
    \]
    com $\xi := \frac{s}{r} < 1$.
    Logo, como $s < r < \alpha$ foram arbitrários, $\sum_{n} a_n z^n$ é dominada pela série geométrica;
    logo converge para todo $z$ com $\norm{z} < \alpha$.
    Isto é, $R \geq \alpha$.

    Se $\norm{z} = s > r > \alpha$, então existe $N$ tal que $\norm{\frac{a_n}{a_{n+1}}} > r$ para todo $n \geq N$.
    Logo, semelhantemente,
    \[
      \norm{a_n z^n}
      =
      \frac{\norm{a_n}}{\norm{a_{n-1}}}
      \cdots
      \frac{\norm{a_{N+1}}}{\norm{a_N}}
      \norm{a_N}
      \norm{z^n}
      >
      \norm{a_N}
      \xi^n
    \]
    com $\xi := \frac{s}{r} > 1$.
    Logo diverge.
    Isto é, $R \leq \alpha$.
  \end{proof}

  \begin{obs}
    Cautela, nada se diz sobre a convergência ou divergência no círculo $\norm{z} = R$.
    Neste caso depende da série específica se ela converge ou não!
  \end{obs}

  Por \cref{prop:raio-convergencia} a série de potências
  \[
    \exp z
    =
    e^z
    \sum_{n} \frac{z^n}{n!}
  \]
  converge para todo $z$ em $\K$.

  \begin{prop}
    Sejam $\sum_{n} a_n$ e $\sum_{n} b_n$ duas séries.
    Se convergem absolutamente, então a série
    \[
      \sum_{n} c_n
      \quad \text{ com } c_n := a_0 b_n + \cdots + a_n b_0
    \]
    converge absolutamente com limite $\sum_{n} a_n \sum_{n} b_n$.
  \end{prop}
  \begin{proof}
    Exercício.
  \end{proof}

  \begin{cor}
    Sejam $\sum_{n} a_n (z-a)^n$ e $\sum_{n} b_n(z-a)^n$ duas séries de potências.
    Se têm raios de convergência $\geq r > 0$, então
    \[
      \sum_{n} c_n (z-a)^n
      \quad \text{ com } c_n := a_0 b_n + \cdots + a_n b_0
    \]
    e
    \[
      \sum_{n} d_n (z-a)^n
      \quad \text{ com } c_n = a_n + b_n
    \]
    tem raio de convergência $\geq r$ com
    \[
      \sum_{n} c_n(z-a)^n
      =
      \sum_{n} a_n(z-a)^n \sum_{n} b_n(z-a)^n
    \]
    e
    \[
      \sum_{n} d_n(z-a)^n
      =
      \sum_{n} a_n(z-a)^n + \sum_{n} b_n(z-a)^n
    \]
    para todo $z \in \K$ com $\norm{z - a} < r$.
  \end{cor}

\subsection{Funções Analíticas}
\label{ssc:funcoes-analiticas}

Seja $\K$ um corpo normado (por exemplo, $\K = \mathbb{R}$ ou $\mathbb{C}$) e $G \subseteq \K$ aberto.

\begin{defn}
  Uma função $f \colon G \to \K$ é \emph{diferençável em um ponto $a$ em $G$} se
  \[
    \lim_{h \to 0}
    \frac{f(a+h) - f(a)}{h}
  \]
  existe, isto é, existe $f'(a)$ em $\K$ tal que, para todo $\epsilon > 0$, existe $\delta > 0$ tal que $\norm{\frac{f(a+h) - f(a)}{h} - f'(a)} < \epsilon$ para todo $h$ com $\norm{h} < \delta$.

  Ela é \emph{diferençável} se é diferençável em todo $a$ em $G$.
\end{defn}


% ex: set spelllang=pt:
