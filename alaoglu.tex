\section{Teorema de Alaoglu}
\label{sc:teorema_de_alaoglu}

Seja $\K$ um corpo.
Um \emph{funcional} sobre um espaço vetorial normado $V$ sobre um corpo $\K$ é uma aplicação linear e contínua $f \from V \to \K$.
Denote
\[
  V^* := \{ \text{ todos os funcionais } f \colon V \to \K \}.
\]

\begin{ex}
  Por exemplo, se $K$ é não-arquimediano e $V$ consiste das sequências de nulo,
  \[
    V = \fct_0(\N)
    :=
    \{ (a_n : n \in \N ) : a_n \to n \}
  \]
  então $V^*$ consiste das sequências limitadas,
  \[
    V^* = \fct^b(\N)
    :=
    \{ (a_n : n \in \N ) : \sup \{a_n \} < \infty \}
  \]
\end{ex}

A topologia \emph{fraca} de $V$ é a topologia inicial dos funcionais sobre $V$;
isto é, a menor topologia tal que todos os funcionais $f \colon V \to \K$ sejam contínuas;
isto é, a topologia gerada pelas pré-imagens $f^{-1} U$ para os funcionais $f \colon V \to \K$ e conjuntos abertos $U$ em $\K$.

\begin{ex}
  Por exemplo, se $V = \fct_0(\N)$, então $(v_n)$ converge para a topologia fraca se, e tão-somente se, $(v_n)$ é limitada e converge em cada coordenada.
\end{ex}

A topologia \emph{fraca$^*$} de $V^*$ é a topologia inicial das avaliações $f \mapsto f(v)$ para todos os $v$ em $V$;
isto é, a menor topologia tal que todas as avaliações $e_v f \mapsto f(v)$ sejam contínuas;
isto é, a topologia gerada pelas pré-imagens $e_v^{-1} U$ para as avaliações $e_v \colon V \to \K$ e conjuntos abertos $U$ em $\K$.

\begin{ex}
  Por exemplo, se $V = \fct_0(\N)$ e $V^* = \fct^b(\N)$, então $(f_n)$ converge para a topologia fraca$^*$ se, e tão-somente se, $(f_n)$ é limitada e converge em cada coordenada.
\end{ex}

\begin{prop}
  Seja $V$ um espaço vetorial topológico.
  Se $V$ é um espaço de Banach, então a topologia \emph{fraca$^*$} de $V^*$ é igual à topologia \emph{fraca} de $V^*$.
\end{prop}
\begin{proof}
  Por \cref{lem:duplo-dual-injetor} e \cref{lem:duplo-dual-sobrejeto-completo}, Se $V$ é um espaço de Banach, então o homomorfismo $V \to V^{**}$ dado por $v \mapsto [f \mapsto f(v)]$ é um isomorfismo.
\end{proof}

Define o homomorfismo \emph{canónico} $V \to V^{**}$ entre $V$ o seu dual duplo $V^{**}$ por $v \mapsto [f \mapsto f(v)]$.

\begin{lem}
  \label{lem:duplo-dual-injetor}
  O homomorfismo $V \to V^{**}$ é injetor.
\end{lem}

\begin{defn}
  Um espaço vetorial topológico $V$ é \emph{reflexivo}, se a aplicação $V \hookrightarrow V^{**}$ é sobrejetora.
\end{defn}

\begin{lem}
  \label{lem:duplo-dual-sobrejeto-completo}
  Se $V$ é um espaço vetorial normado e $K$ é esfericamente completo, então $V$ é reflexivo se, e tão-somente se, $V$ é completo.
\end{lem}
\begin{proof}
  Pelo Teorema de Hahn-Banach.
\end{proof}

\begin{lem}
  \label{lem:linear-continua-limitada}
  Uma aplicação linear entre espaços vetoriais normados é contínua se, e somente se, é limitada.
\end{lem}
\begin{proof}
  Se é limitada, em particular é (uniformemente) contínua.

  Seja $f \from V \to W$ ilimitada, isto é, existe uma sequência $x_1$, $x_2$, \ldots (cujas entradas são) em $V$ tal que $\Norm{x_n} \leq 1$ e $\Norm{f(x_1)}$, $\Norm{f(x_2)}$, \ldots é ilimitada.
  Sejam $\lambda_1$, $\lambda_2$, \ldots em $\K$ tal que $\Norm{f(\lambda_1 x_1)}$, $\Norm{f(\lambda_2 x_2)}$, \ldots é limitada e maior do que $1$.
  Como $x_1$, $x_2$, \ldots é limitada, $\lambda_1 x_1$, $\lambda_2 x_2$, \ldots converge a $0$.
  Como $\Norm{f(\lambda_1 x_1)}$, $\Norm{f(\lambda_2 x_2)}$, \ldots $> 1$, $f(x_1)$, $f(x_2)$, \ldots não converge a $f(0) = 0$.
  Em particular, $f$ não é contínua (em $0$).
\end{proof}

A \emph{norma de operador} $\Norm{f}$ de um funcional $f$ é definida por
\[
  \Norm{f}
  :=
  \sup \{ \norm{f(v)} : v \text{ em } V \};
\]
que dá a $V^*$ uma métrica $\dist$ definida por $\dist(f,g) = \norm{f - g}$.
que induz a topologia gerada pela base das bolas abertas $\Ball(x, r) := \{ g \in V^* : \dist(g,f) < \epsilon \}$.
A bola \emph{unitária} $B$ é a bola fechada $\Ball(0, 1) := \{ f \in V^* : \Norm{f} \leq 1 \}$

\begin{defn}
  Um corpo $\K$ é \emph{localmente compacto} se para todo $x$ em $\K$ existe uma vizinhança $V \owns x$ compacta.
\end{defn}

Equivalentemente, um corpo $\K$ é \emph{localmente compacto} se para todo $x$ em $\K$ existe uma vizinhança aberta $V \owns x$ tal que o seu fecho é compacto.

Se a bola unitária de $\K$ é compacta, então $\K$ é localmente compacto.

Por exemplo, $\K = \R$, $\C$ e as extensões finitas de $\Q_p$ e $\F_p((t))$ são localmente compactos (e de fato constituem todos tais corpos).
Já vimos que $\Z_p$, a bola unitária de $\Q_p$, é compacto;
logo $\Q_p$ é localmente compacto (e semelhantemente $\F_p((t))$).

\begin{thm}[de Banach-Alaoglu]
  Seja $\K$ um corpo topológico e $V$ um espaço normado sobre $\K$.
  Se $\K$ é localmente compacto, então a bola unitária fechada de $V^*$ é compacta para a topologia fraca$^*$.
\end{thm}
\begin{proof}
  Seja $B$ a bola unitária fechada de $V$ e seja $B^*$ a bola unitária fechada de $V*$.
  Seja $b$ a bola unitária fechada de $\K$.
  Logo
  \[
    B^* = \{ f \colon B \to b : \text{ linear } \}
    \subseteq
    b^B
  \]
  Pelo Teorema de Tychonoff, o produto $b^B$ de compactos $b$ é compacto para a topologia do produto.
  Como o conjunto dos funcionais $B^*$ é fechado em $b^B$, também é compacto para a topologia do produto.
  Pela sua definição, a topologia do produto é igual à topologia fraca$^*$.
  Logo $B^*$ é compacto para a topologia fraca$^*$.
\end{proof}

% ex: set spelllang=pt:
