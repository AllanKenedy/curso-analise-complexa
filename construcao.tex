\section{Construção dos números complexos}
\label{sc:construcao-dos-numeros-complexos}

  \subsection{Frações}
  \label{ssc:fracoes}

  Um elemento $x$ em um anel $A$ é uma \emph{unidade}, ou \emph{invertível}, se existe $y$, denotado por $y = x^{-1}$, tal que $x y = 1$.
  Um \emph{corpo} é um anel em que todo elemento é invertível.
  % Por exemplo, $\Q$ e $\R$ são corpos, e também, por \cref{cor:FpCorpo}, $\F_p = \Z / p \Z$.

  Seja $A$ um anel comutativo sem divisores de $0$, isto é, não existem $x$ e $y$ diferentes de $0$ em $A$ tais que $xy = 0$.
  Por exemplo, além dos corpos, $\Z$ e $\Z_p$ são tais anéis.

  O \emph{corpo das frações} $Q$ de um tal $A$ é o \emph{menor} corpo que contém $A$ (isto é, existe $A \to Q$ e, para qualquer outro corpo $R$ com $A \to R$, existe $Q \to A$ que a fatore, isto é, tal que $A \to R = A \to Q \to R$).

  Ele é construído como conjunto por
  \[
    Q = A \x A / \sim
  \]
  onde duas "frações" $(x', y')$ e $(x'', y'')$ são equivalentes se uma se simplifica a outra, isto é, $(x',y') \sim (x'', y'')$ se existe $a$ em $A$ tal que $a (x', y') = (a x', a y') = (x'', y'')$.
  A classe de equivalência de $(x,y)$ em $Q$ é denotada por $x/y$.
  Como anel, a adição e multiplicação é a de $A$ em cada coordenada.

  \begin{defn*}
    Seja
    \[
      \Q
      :=
      \text{ corpo das frações de $\Z$ }
      \quad \text{ e } \quad
      \norm{x/y}_p := \norm{x} / \norm{y}.
    \]
  \end{defn*}

  \subsection{Completamento}

  Em $\R$, uma expansão decimal $a_0 + a_1 10^{-1} + \dotsb$ com $a_0$, $a_1$, \ldots em $ \{ 0, 1, \ldots, 9 \} $ converge com respeito ao valor absoluto usual $\norm{\cdot}$.

  Seja $A$ um anel.
  Um \emph{valor absoluto} (restrito) em $A$ é uma aplicação $\norm{\cdot} \from A \to \Q_{\geq 0}$ tal que
  \begin{itemize}
    \item
      (Hausdorff) $\norm{x} = 0 \text{ se, e somente se, } x = 0$,
    \item
      (Multiplicatividade) $\norm{xy} = \norm{x}\norm{y}$, e
    \item
      (Desigualdade Triangular) $\norm{x + y} \leq \norm{x} + \norm{y}$.
  \end{itemize}

  Ao invés de valor absoluto, usa-se também o termo \emph{norma}.
  Chamamos um anel munido de uma tal norma de anel \emph{normado}.

  % \begin{rem*}
  % O \emph{tipo} de um valor absoluto é o conjunto das potências $\{\norm{\cdot}^e:e > 0\}$ de um valor absoluto
  % $\norm{\cdot}$, isto é todas as normas que definem a mesma topologia.
  % \end{rem*}

  \begin{defn*}
    Seja $X$ um conjunto.
    Uma aplicação $\dist \from X \times X \to [0, \infty[$ é uma função de \emph{distância} ou \emph{métrica} se
    \begin{itemize}
      \item
        $d(x,y) = 0$ se, e somente se, $x = y$,
      \item
        $d(x,y) = d(y,x)$, e
      \item
        $d(x,z) \leq d(x,y) + d(y,z)$.
    \end{itemize}
    Um \emph{espaço métrico} é um par $(X, d)$ de um conjunto $X$ e uma função distância $d$ como acima.

    Uma aplicação $f$ entre espaços métricos é \emph{uniformemente contínua} se
    \[
      \text{para todo } \epsilon > 0,
      \text{ existe } \delta > 0 \text{ tal que }
      \dist(x,y) < \delta \text{ implica }
      \dist(f(x), f(y)) < \epsilon.
    \]
  \end{defn*}

  Como as aplicações que respeitam a estrutura entre espaços topológicos são as aplicações \emph{contínuas} e entre espaços vetoriais as aplicações \emph{lineares}, aqui, entre espaços métricos, supomos todas as aplicações \emph{uniformemente contínuas}.

  Se $\norm{\cdot}$ é uma norma sobre um anel, então
  \[
    d(x,y) := \norm{x-y}
  \]
  é uma função distância.

  \begin{defn*}
    Uma sequência $(x_n)$ em um espaço métrico é dita de \emph{Cauchy} se para todo $\epsilon > 0$, existe $N$ tal que $\dist(x_n, x_m) < \epsilon$ para todos os $n,m > N$.
    Um espaço métrico é \emph{completo} se toda sequência de Cauchy converge.
  \end{defn*}

  Em particular,  toda sequência que converge é Cauchy.

  \begin{thm}[Completamento]
    \label{thm:completamento}
    Para todo espaço métrico $X$ existe um (único) espaço métrico completo $\Xh$ com uma aplicação (uniformemente contínua) $X \to \Xh$ tais que para qualquer espaço métrico completo $Y$ com uma aplicação (uniformemente contínua) $X \to Y$, existe uma aplicação (uniformemente contínua) $\Xh \to Y$ que a fatora, isto é
    \begin{ccd}
      X \ar{r} \ar{d} & Y \\
      \Xh \ar{ru} &
    \end{ccd}
  \end{thm}
  \begin{proof}
    Definimos o conjunto
    \[
      \Xh
      :=
      \{ \text{ sequências de Cauchy em $X$ } \} / \sim
    \]
    (e no qual $X$ se injeta pelas sequências constantes) onde a relação de equivalência $\sim$ é definida por $(x_n) \sim (y_n)$ se $\dist(x_n, y_n) \to 0$.
    A função distância $\hat \dist$ é definida por
    \[
      {\hat \dist}([(x_n)], [(y_n)])
      :=
      \lim \dist(x_n, y_n)
    \]
    para representantes $(x_n)$ e $(y_n)$ das classes de equivalência $[(x_n)]$ e $[(y_n)]$.
    Observe que é bem-definida, isto é, independente dos representantes das classes de equivalência.
    Se $(x_n)$ é uma sequência de Cauchy (de classes de equivalência de sequências de Cauchy) com $x_n = [(x_{n,m} : m \in \N)]$, então ela converge a sequência diagonal $x = (x_{n, n} : n \in \N)$.
    (O leitor é convidado a convencer-se da existência da aplicação $\Xh \to Y$.)
  \end{proof}

  \begin{obs*}
    Uma abordagem a construir o espaço métrico $\R$ é como completamento de $\Q$ para a função distância usual:
    Se completamos $\Q$ a $\R$, observamos que
    \begin{itemize}
      \item a função distância tem imagem em $\Q_{\geq 0} = \{ x \in \Q : x \geq 0 \}$, e
      \item a função distância sobre $\R = \Qh$ precisa de ser definida, como ainda não construímos $\R$, por
        \[
          \hat \dist([(x_n)], [(y_n)])
          :=
          [(\dist(x_n, y_n))].
        \]
        Observe que $[(\dist(x_n, y_n))] = \lim \dist(x_n, y_n)$ pela definição do limite como sequência diagonal (dos representantes) das sequências constantes cujas entradas todas são $\dist(x_n, y_n)$.
    \end{itemize}
  \end{obs*}

  Revela-se por construção
  \begin{itemize}
    \item que a aplicação $X \to \Xh$ é injetora, e
    \item
      que $X \text{ é \emph{denso} em } \Xh$;
      isto é, para todo $\hat x$ em $\Xh$ existem $x_1$, $x_2$, \ldots em $X$ tal que $x_1, x_2, \ldots \to \hat x$, ou
      \begin{itemize}
        \item
          mais formalmente:
          para qualquer $\epsilon > 0$ e $\hat x$ em $\Xh$ existe $x$ em $X$ tal que $\dist(\hat x, x) < \epsilon$;
        \item
          informalmente:
          todos os elementos no completamento são limites do espaço completado.
      \end{itemize}
  \end{itemize}
  \begin{proof}
    \hfill
    \begin{itemize}
      \item
        Sejam $x$ e $y$ em $X$ e $(x_n) = (x, x, \ldots)$ e $(y_n) = (y, y, \ldots)$ representantes dos seus valores sob $X \to \Xh$.
        Vale $(x_n) = (y_n)$ se, e tão-somente se, $\hat \dist((x_n), (y_n)) = \dist(x,y) = 0$, isto é, $x = y$.
      \item
        % $\Xh$ é o \emph{menor} espaço completo que inclui $X$ entre todos tais espaços completos (no sentido que qualquer outro tal espaço permite uma aplicação de $\Xh$ nele)
        Dado $\epsilon > 0$ e $\bar x$ em $\Xh$ representado por uma sequência de Cauchy $x = (x_n)$ em $X$, escolhe $N$ tal que $\dist(x_n, x_m) < \epsilon$ para todos os $n,n m > N$.
        Seja $y := (x_N, x_N, \ldots)$ a sequência constante e $\bar y$ a sua classe residual em $\Xh$.
        Logo,
        \[
          \hat\dist(\bar x, \bar y)
          =
          \lim_n \dist(x_n, y)
          =
          \lim_n \dist(x_n, x_N)
          \leq
          \max \{ \dist(x_m, x_N) : m \geq N \}
          <
          \epsilon.
          \qedhere
        \]
    \end{itemize}
  \end{proof}

  \begin{obs*}[ou digressão para o dia-a-dia do matemático]
    A construção do completamento $\Xh$ para $X$, como a de $\Z / m \Z$ para $m$ em $\N$ (como a de $\Q$ a partir de $\Z$), é uma construção \emph{universal} por classes residuais.
    No final só importa
    \begin{itemize}
      \item
        que $\Xh$ seja o "menor" espaço métrico em que toda sequência de Cauchy converge, e
      \item
        que $\Z / m \Z$ seja o "menor" anel em que $m (= 1 + \dotsb + 1) = 0$, e
      \item
        que $\Q$ seja o "menor" anel em que todo número inteiro é invertível.
    \end{itemize}

    Basta-nos saber que todos os limites de sequências que convergem já estão em $\Xh$.
    A construção é teoricamente importante, mas, uma vez feita, é praticamente deixada de lado.
    (Como ninguém pensa em uma classe residual de números racionais ao ver um número real.)
    % --- e, a sua volta, em classes residuais de pares de números inteiros ao ver um número racional
    \todo{Corte ao mínimo! Enxuga!}
  \end{obs*}

  \subsection{Extensão Algébrica}
  \label{ssc:extensao-algebrica}

  Um elemento $\alpha$ na extensão $E$ de um corpo $F$ é \emph{algébrico} se existe $P(X) \in F[X]$ tal que $P(\alpha) = 0$.
  Entre todos os tais $P$ com $P(\alpha) = 0$ existe um único polinômio $M(X) = X^n + a_{n-1} X^{n-1} + \dotsb + a_0$ de grau mínimo e cujo coeficiente dominante é igual a $1$, o \emph{polinômio mínimo} de $\alpha$.
  A extensão $E$ de um corpo $F$ é \emph{algébrica} se todo elemento é algébrico;
  equivalentemente, se é gerada por elementos algébricos.
  Em particular, $E$ é uma extensão algébrica finitamente gerada de $F$, se, e tão-somente se, o espaço vetorial $E$ tem dimensão finita sobre $F$;
  chamemos uma tal extensão de extensão \emph{finita} e denote $[E : F] = \dim_F E$.

  \begin{prop}
    Seja $E$ uma extensão algébrica de $F$.
    Se é gerada por um elemento $\alpha$ com polinômio mínimo $M$, então
    \[
      F[X]/M F[X] \iso E
    \]
    com $X \mapsto \alpha$.
  \end{prop}
  \begin{proof}
    É sobrejetor porque a imagem contém $F$ e $\alpha$.
    O núcleo do homomorfismo $F[X] \to E$ é um ideal que anula $\alpha$.
    É principal, porque $F[X]$ é euclidiano pela divisão com resto;
    por definição o núcleo gerado é por $M$
  \end{proof}

  \begin{obs}
    Temos
    \[
      F \oplus F \cdot X \oplus \dotsb \oplus F \cdot X^{m-1} \iso F[X]/M F[X]
    \]
    onde $m$ = grau de $M$.
  \end{obs}
  \begin{proof}
    É injetor porque $m-1 < m$ = grau de $M$.
    E sobrejetor porque pela divisão com resto todo polinómio $P$ se escreve como
    \[
      P = Q M + R
    \]
    com grau de $R < m$ = grau de $M$.
  \end{proof}

  Seja $F = \R$ e $M = X^2 + 1$.
  É irredutível, porque, pela divisão com resto, $M$ é redutível se, e tão-somente se, tem zero.

  \begin{defn}
    Seja
    \[
      \C = \R[X] / (X^+1) \R[X].
    \]
    Pela observação, $\C \iso \R \oplus \R X$.
    Denote $X$ por $i$, da forma que $i$ é uma solução de $X^2 + 1$, isto é, $i^2 = -1$.
  \end{defn}

  Define uma norma $\C$ sobre $\R$ pela identificação de $\C = \R \oplus \R$ como espaço vetorial bidimensional;
  isto é, $\norm{ x + iy } = x^2 + y^2$.

  Existe um único homomorfismo não-trivial da álgebra $\C$ sobre $\R$ dada pela conjugação
  \[
    z = x + iy \mapsto \bar z := x - iy.
  \]

% ex: set spelllang=pt:
