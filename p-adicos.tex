\section{O Teorema de Ostrowski}
\label{sc:o-teorema-de-ostrowski}

  \subsection{Números $p$-ádicos}
  \label{ssc:numeros-p-adicos}

  Seja $A$ um anel.
  Um \emph{valor absoluto} em $A$ é uma aplicação $\norm{\cdot} \from A \to \R_{\geq 0}$ tal que
  \begin{itemize}
    \item (Hausdorff) $\norm{x} = 0 \text{ se, e somente se, } x = 0$,
    \item (Multiplicatividade) $\norm{xy} = \norm{x}\norm{y}$, e
    \item (Desigualdade Triangular) $\norm{x + y} \leq \norm{x} + \norm{y}$.
  \end{itemize}

  Ao invés de valor absoluto, usa-se também o termo \emph{norma}.
  Chamamos um anel munido de uma tal norma de anel \emph{normado}.

  Seja $p \in \{ 2, 3, 5, 7, 11, \ldots \}$ um número primo.

  \begin{defn*}
    Seja
    \[
      \norm{\cdot}_p \from \Z \to \R_{\geq 0}
    \]
    a norma \emph{$p$-ádica} definida por $\norm{0}_p \coloneqq 0$ e
    % $\norm{0}_p = 0$ e
    \[
      \norm{a}_p \coloneqq p^{-v_p(a)},
      \text{ onde }
      p^{v_p(a)} \text{ é a maior potência de } p \text{ que divide } a.
  \]
  \end{defn*}

  \begin{ex*}
    \hfill
    \begin{itemize}
      \item Para $p = 2$ temos que $\norm{8}_2 = \norm{2^3}_2 = 2^{-3}$ e $\norm{9}_2 = \norm{9 \cdot 2^0}_2 = 2^{-0} = 1$;
      \item para $p = 3$ temos que $\norm{8}_3 = \norm{3^0 \cdot 8}_3 = 3^{-0} = 1$ e $\norm{9}_3 = \norm{3^2}_3 = 3^{-2}$.
    \end{itemize}
  \end{ex*}

  A norma $p$-ádica $\norm{\cdot}_p$ mede quantas vezes $p$ aparece na fatoração de um número inteiro.
  Contra-intuitivamente para quem se acostumou à norma usual, a norma $p$-ádica \emph{diminui} quando a potência de $p$ \emph{cresce};
  um número inteiro grande (com respeito à norma usual) pode ter norma $p$-ádica pequena.

  \begin{rem*}
    A contra-intuição da norma p-ádica $\norm{\cdot}_p$, em comparação à norma usual, é que ela é \emph{não-arquimediana}, isto é
    \[
      \norm{\underbrace{1 + \dotsb +1}_{n \text{ vezes}}} \leq 1
      \quad \text{ para todo $n$}.
    \]
  \end{rem*}

  Apesar da sua natureza aparentemente exótica, \cref{thm:ostrowski} mostra que as únicas normas sobre $\Q$ são a norma usual e as normas $p$-ádicas.

  Os números $p$-ádicos são relativamente recentes, em comparação aos números reais;
  foram introduzidos há cerca de cem anos por Kurt Hensel:
  Em analogia a $\R$, que consiste de todos os limites de $\Q$ para a norma usual $\norm{\cdot}$, vamos definir $\Z_p$ como o conjunto dos limites de $\Z$ para a norma $p$-ádica $\norm{\cdot}_p$.
  Formalmente, $\Z_p$ é o \emph{completamento} de $\Z$ para $\norm{\cdot}_p$.

  \begin{defn*}
    O anel normado dos \emph{números p-ádicos inteiros} é definido por
    \[
      \Z_p
      :=
      \text{ o completamento de $\Z$ com respeito à norma $p$-ádica $\norm{\cdot}_p$ }.
    \]
  \end{defn*}

  Em particular, como $\norm{p^n}_p = p^{-n}$ e $\norm{a_n}_p = 1$ para $a_n$ em $\{1, \ldots, p-1\}$, vale $\norm{a_n p^n}_p = p^{-n}$ e
  \[
    \norm{a_n p^n + a_{n + 1} p^{n + 1} + \dotsb + a_N p^N }_p
    =
    p^{-n} \to 0 \quad \text{ para } n \to \infty.
  \]
  Isto é, a sequência $(a_0, a_0 + a_1 p, a_0 + a_1 p + a_2 p^2, \ldots)$ é Cauchy.
  Como $\Z_p$ é completo, ela converge, isto é, a série infinita $a_0 + a_1 p + a_2 p^2 + \dotsb$ converge.
  Isto é, em $\Z_p$, todo número $a = a_0 + a_1 p + a_2 p^2 + \dotsb$ existe como limites de números em $\Z$, como em $\R$ todo número $b = b_0 + b_1 10^{-1} + b_2 10^{-2} + \dotsb$ para $b_0$, $b_1$, \ldots em $\{0,1, \ldots, 9\}$ existe como limites de números em $\Q$.

  Uma vez que $\Z$ é denso em $\Z_p$, as funções $+$, $\cdot$ e $\norm{\cdot}_p$ de $\Z$ uniformemente contínuas estendem-se ao completamento $\Z_p$.
  (Se $f \from X \to Y$ é uma função de $X$ cujo contra-domínio $Y$ é completo, então $f(x) := \lim x_n$ para $x_n \to \hat x$ em $\Xh$ estende $f$ a $\Xh$.)
  Por isso, $\Z_p$ é verdadeiramente um anel com um valor absoluto (e não somente um espaço métrico).

  \begin{figure}
    \centering
    \includegraphics[width = 0.6\textwidth]{./images/binary_tree.png}
    \caption{A árvore binária que representa $\Z_2$}
    \label{fig:arvore-binaria}
  \end{figure}

  A árvore binária da \cref{fig:arvore-binaria} representa $\Z_2$ da seguinte maneira:
  Cada número $p$-ádico tem uma expansão $p$-ádica dada pelos seus coeficientes, sendo ou $0$, ou $1$, e conforme a estes coeficientes, pega, ou o ramo da esquerda, ou o da direita em uma bifurcação (ou nó) da árvore.

  Reflitamos como se descreve o valor absoluto, a função distância e as bolas sobre eles:

  \begin{itemize}
    \item
      Vale $\dist(x,y)_p = \norm{x-y}_p = p^{-v}$ onde $v$ = o nível em que os dois ramos infinitos $x$ e $y$ bifurcam;
      em particular, vale $\norm{x}_p = p^{-v}$ onde $v$ = o nível da primeira bifurcação em que o ramo infinito vai à direita.
    \item
      Uma bola corresponde a uma malha da árvore por $\Ball(x, p^{-n}) \mapsto x_0 + x_1 p + \dotsb + x_n p^n$ se $x = x_0 + x_1 p + \dotsb$, onde $\Ball(x, p^{-n})$ é a bola com centro $x$ e raio $p^{-n}$ e $x_0 + x_1 p + \dotsb + x_n p^n$ é o ramo finito que leva à malha;
      a bola é dada por todos os ramos infinitos que passam pela malha correspondente.
      Observamos que duas bolas, ou se incluem, ou são disjuntas!
      Isto é, em termos topológicos, $\Z_p$ é \emph{totalmente desconexo}.
  \end{itemize}

  \begin{defn*}
    Seja
    \[
      \Q_p
      :=
      \text{ corpo das frações de $\Z_p$ }
      \quad \text{ e } \quad
      \norm{x/y}_p := \norm{x}_p / \norm{y}_p
    \]
    o corpo normado dos \emph{números $p$-ádicos}.
  \end{defn*}

  \begin{prop}
    \label{prop:MergulhoZpModpZp}
    O mergulho $\Z \to \Z_p$ induz para todo $n$ em $\N$ um isomorfismo de anéis
    \[
      \Z / p^n \Z
      \iso
      \Z_p / p^n \Z_p.
    \]
  \end{prop}
  \begin{proof}
    Como a aplicação $\Z \to \Z_p \to \Z_p / p^n \Z_p$ tem núcleo $p^n \Z$, obtemos a injeção $\Z / p^n \Z \to \Z_p / p^n \Z$.
    Ela é sobrejetora se, e somente se, dado $\hat x$ em $\Z_p$ e $n$ em $\N$, existe $x$ em $\Z$ tal que $\norm{\hat x - x}_p \leq p^{-n}$.
    Como $\Z$ é denso no seu completamento $\Z_p$, em particular tal $x$ existe.
  \end{proof}

  Para um anel $A$, denote $A^*$ o grupo multiplicativo das suas unidades.
  Por exemplo, $\Z^* = \{ \pm 1 \}$ e $\Q^* = \Q - \{ 0 \}$.

  \begin{prop}
    \label{prop:UnidadesZp}
    Vale
    \[
      \Z_p^* = \Z_p - p \Z_p.
    \]
    Isto é, $x$ em $\Z_p$ é invertível se, e somente se, $\norm{x} = 1$.
  \end{prop}
  \begin{proof}
    Seja $x$ em $\Z_p$.
    Se $x$ é invertível, isto é, existe $y$ em $\Z_p$ tal que $xy = 1$, em particular $1 = \norm{1}_p = \norm{xy}_p = \norm{x}_p \norm{y}_p$ e logo $\norm{x}_p = 1$.

    Seja $x$ em $\Z_p - p \Z_p$.
    Pela \cref{prop:MergulhoZpModpZp} e pelo \cref{cor:FpCorpo},
    \[
      \Z_p / p \Z_p
      =
      \Z / p \Z
      =
      \F_p
    \]
    é um corpo, isto é, existe $a$ em $ \{ 1, \ldots, p-1 \} $ tal que $ax$ em $1 + p \Z_p$, isto é, $\norm{ax - 1}_p < 1$.
    Pela série geométrica $1 + \alpha + \alpha^2 + \dotsb \to 1/(1-\alpha)$, existe $(ax)^{-1}$, e em particular $x^{-1} = a (ax)^{-1}$.
  \end{proof}

  Recordemos que um \emph{ideal} $I$ em um anel $A$ é um subconjunto $I$ em $A$ tal que
  \begin{itemize}
    \item
      para $x$ e $y$ em $I$, vale $x + y$ em $I$, e
    \item
      para $x$ em $I$ e $y$ em $A$, vale $xy$ em $I$.
  \end{itemize}
  Isto é, em comparação a um subanel, o ideal é fechado sob multiplicações por qualquer elemento do anel inteiro, não só do subanel.

  Um ideal é \emph{máximo} se não existe outro ideal, distinto do anel todo, que o contenha.

  \begin{cor*}
    \hfill
    \begin{itemize}
      \item O único ideal máximo de $\Z_p$ é $p \Z_p$.
      \item
        $
          \{ \text{ ideais em $\Z_p$ } \}
          =
          \{ \Z_p, p \Z_p, p^2 \Z_p, \ldots \} \cup \{ 0 \}.
        $
      \item
        $
          \Q_p
          =
          \Z_p[1/p]
          =
          \dot\bigcup_{n \in \Z} p^n \Z_p^* \cup \{ 0 \}.
        $
    \end{itemize}
  \end{cor*}
  \begin{proof}
    % Observamos que um elemento $x$ em $A$ não é uma unidade em $A$ se, e somente se, $x$ pertence a algum ideal máximo.
    \hfill
    \begin{itemize}
      \item
        Seja $A$ um anel.
        O complemento $A-I$ de todo ideal $I$ contém as unidades $A^*$.
        Logo, pela \cref{prop:UnidadesZp}, se $I$ é um ideal em $\Z_p$, logo $\Z_p - I \supseteq Z_p^* = Z_p - p \Z_p$, ou, equivalentemente, $I \subseteq p \Z_p$;
        isto é, $p \Z_p$ é o único ideal máximo.
      \item
        Seja $x$ em $\Z_p$ com $\norm{x} = p^{-n}$, isto é, $x = p^n a$ com $\norm{a} = 1$.
        Pela \cref{prop:UnidadesZp}, $a$ em $\Z_p^*$, isto é, existe $b$ em $\Z_p$ tal que $ab = 1$, ou, equivalentemente, $\Z_p x = p^{-n} \Z_p$.
        Concluímos que um ideal $I$ em $\Z_p$ é gerado pelo seu elemento do mínimo valor, isto é, $I = p^n \Z_p$ onde $p^n$ é a maior potência de $p$ que divide todos os elementos em $I$.
      \item
        Pela \cref{prop:UnidadesZp}, só falta inverter $p$ em $\Z_p$ para poder inverter todos os elementos em $\Z_p$.
        O corpo $\Q_p$ é o menor anel em que todos os elementos em $\Z_p$ são invertíveis, logo $\Q_p = \Z_p[1/p]$.
        \qedhere
    \end{itemize}
  \end{proof}

  Por isso, se voltamos à definição de $\Z_p$ como conjunto de expansões $p$-ádicas infinitas,
  \[
    \Q_p
    =
    \{ p^{-n} a_{-n} p^{-n} + \dotsb + a_0 + a_1 p^1 + \dotsb
    \text{ para } n \text{ em } \N \text{ e } a_{-n}, \ldots \in \{0,\ldots,p-1\} \}.
  \]
  \begin{obs*}
    Duas séries iguais com entradas racionais podem convergir a limites diferentes em $\Q_p$ e $\R$.
    Por exemplo, com $\binom{x}{n} := x(x-1) \dotsm (x-n+1)/n!$, a série dada por
    \[
      \sqrt[2]{16/9}
      =
      (1 + 7/9)^{1/2}
      =
      \sum_{n \geq 0} \binom{1/2}{n} (7/9)^n
    \]
    converge à raiz positiva $4/3$ em $\R$ e a raiz negativa em $\Q_7$ (vide \cite[Capítulo IV, Prova do Non-Theorem 1]{Koblitz:Analysis})!
  \end{obs*}

  \subsection{O Teorema de Ostrowski}
  \label{sc:o-teorema-de-ostrowski}

\begin{defn*}
  Duas funções distância são \emph{(Cauchy-)equivalentes} se elas têm as mesmas sequências de Cauchy.
  Isto é, duas funções distância $d'$ e $d''$ sobre um conjunto $X$ são equivalentes se para todo $\epsilon > 0$ existe $\delta$ tais que, para todos os $x,y$ em $X$, se $d'(x,y) < \delta$ então $d''(x,y) < \epsilon$ e se $d''(x,y) < \delta$ então $d'(x,y) < \epsilon$
\end{defn*}

\begin{obs*}
  Dados dois valores absolutos $\norm{\cdot}_{'}$ e $\norm{\cdot}_{''}$ sobre um corpo, as métricas induzidas são (Cauchy-)equivalentes se, e somente se, existe um $c > 0$ tal que $\norm{\cdot}_{''} = {\norm{\cdot}_{'}}^c$.
\end{obs*}

O valor absoluto $\norm{\cdot}$ é \emph{trivial} se $\norm{0} = 0$ e $\norm{\cdot} = 1$ senão.

\begin{thm}[Ostrowski]
  \label{thm:ostrowski}
  Todo valor absoluto não trivial sobre $\Q$ é (Cauchy-)equivalente
  \begin{itemize}
    \item
      ou ao valor absoluto usual $\norm{\cdot}$,
    \item
      ou a um valor absoluto p-ádico $\norm{\cdot}_p$ para um número primo $p$.
  \end{itemize}
\end{thm}
\begin{proof}
  Seja $\norm{\cdot}$ um valor absoluto sobre $\Q$.
  Distinguimos dois casos, o caso \emph{arquimediano} e \emph{não-arquimediano}.
  Como $\norm{-1} = 1$ e $\norm{x/y} = \norm{x} / \norm{y}$, basta verificar sobre $\N$ que existe $\alpha > 0$ tal que $\norm{\cdot}^\alpha$ é ou igual ao valor absoluto usual, ou a um $\norm{\cdot}_p$ para $p$ um número primo.
  \bigbreak

  \emph{Caso arquimediano}: Existe um $n$ em $\N$ tal que $\norm{n} > 1$.

  Seja $n_0$ o menor tal $n$.
  (Por exemplo, se $\norm{\cdot}$ é o valor absoluto usual, então $n_0 = 2$.)
  Como $\norm{n_0} > 1$, existe $\alpha > 0$ tal que $\norm{n_0} = n_0^\alpha$.

  Expanda $n$ na base $n_0$, isto é
  \[
    n
    =
    a_0 + a_1 n_0 + \dotsb + a_s n_0^s
    \quad \text{ com } a_0, \ldots, a_s \text{ em } \{ 0, \ldots, n_0-1 \}.
  \]
  Segue
  \begin{align}
    \norm{n} & = \norm{a_0 + a_1 n_0 + \dotsb + a_s n_0^s}\\
             & \leq \norm{a_0} + \norm{a_1} \norm{n_0} + \dotsb + \norm{a_s} \norm{n_0}^s
  \end{align}
  Como $a_0$, \ldots, $a_s < n_0$, pela escolha de $n_0$, vale $\norm{a_0}$, \ldots, $\norm{a_s} \leq 1$.
  Segue
  \begin{align}
    \norm{a_0} + \norm{a_1} \norm{n_0} + \dotsb + \norm{a_s} \norm{n_0}^s \leq & 1 + n_0^\alpha + \dotsb + n_0^{\alpha s}\\
    = & n_0^{s \alpha} (1 + n_0^{- \alpha} + \dotsb + (n_0^{-\alpha})^s)
  \end{align}
  Esta soma é limitada pela série geométrica:
  Pondo $c = n_0^{-\alpha} < 1$, vale
  \begin{align}
    1 + n_0^{- \alpha} + \dotsb + (n_0^{-\alpha})^s & = 1 + c + \dotsb + c^s \\
                                                    & \leq 1 + c + c^2 + \dotsb = \frac{1}{1-c} =: C
  \end{align}
  com $C = C(\alpha, n_0)$ independente de $n$.
  Como $n_0^s \leq n$, vale $\norm{n} \leq n^\alpha C$.
  Segue, para todo $N$ em $\N$
  \[
    \norm{n}^N = \norm{n^N} \leq (n^\alpha)^N C,
  \]
  extraindo a raiz de índice $N$
  \[
    \norm{n} \leq n^\alpha \sqrt[N]{C},
  \]
  e como isto vale para $N$ arbitrariamente grande
  \[
    \norm{n} \leq n^\alpha.
    \tag{$*$}
  \]
  Para ver a desigualdade $\norm{n} \geq n^\alpha$ oposta a $(*)$, observe
  \[
    \norm{n_0^{s+1}}
    =
    \norm{n_0^{s + 1} - n + n}
    \leq
    \norm{n_0^{s + 1} - n} + \norm{n},
  \]
  e consequentemente, pela desigualdade obtida $(*)$,
  \[
    \norm{n}
    \geq
    \norm{n_0^{s + 1}} - \norm{n_0^{s + 1} - n}
    \geq
    n_0^{\alpha(s + 1)} - (n_0^{s + 1} - n)^\alpha.
  \]
  Como $n_0^s \leq n \leq n_0^{s + 1}$, segue
  \begin{align}
    \norm{n} & \geq (n_0^{s + 1})^\alpha - (n_0^{s + 1} - n_0^s)^\alpha\\
             & = (n_0^{s + 1})^\alpha \left(1 - (1- \frac{1}{n_0})^\alpha\right) \geq n^\alpha D
  \end{align}
  com $D = D(\alpha, n_0) := 1 - (1- \frac{1}{n_0})^\alpha$ independente de $n$.
  Como acima, segue $\norm{n} \geq n^\alpha$ concluindo com $(*)$ que $n = n^\alpha$ e assim o caso arquimediano.
  \bigbreak

  \emph{Caso não-arquimediano}: Para todos os $n$ em $\N$ vale $\norm{n} \leq 1$.

  Como $\norm{\cdot}$ não é trivial, existe $n$ em $\N$ tal que $n < 1$.
  Seja $n_0$ o menor tal $n$.
  Como $\norm{\cdot}$ é multiplicativo, necessariamente $n_0 = p$ primo.
  \bigbreak

  \emph{Proposição}: Para todo número primo $q \neq p$ vale $\norm{q} = 1$.

  \emph{Demonstração}: Caso contrário, existe $N$ tal que $\norm{q^N}, \norm{p^N} < 1/2$.
  Pelo \cref{thm:EuclidesEstendido}, os números $q^N$ e $p^N$ são relativamente primos se, e somente se, $ \langle q^N \rangle + \langle p^N \rangle = \langle 1 \rangle$, isto é, existem $m,n$ tais que
  \[
    m q^N + n p^M
    =
    1.
  \]
  Segue a contradição
  \[
    1
    =
    \norm{1}
    =
    \norm{m} \norm{q^N} + \norm{n} \norm{p^M}
    <
    1/2 + 1/2
    =
    1.
  \]
  \bigbreak

  Seja $x$ em $\N$.
  Se $x = p_1^{x_1} \dotsm p_r^{x_r}$ é a fatoração em números primos e $p_{i_0} = p$, então $\norm{x} = \norm{p_{i_0}}^{x_{i_0}}$.
  Isto é, $\norm{x} = c^{v_p(x)}$ com $c = \norm{p}$ e $v_p(x)$ = o maior $n$ tal que $p^n$ divide $x$.

  Se escolhemos $\alpha$ tal que $c = \norm{p} = \norm{p}_p^\alpha = p^{-\alpha}$, isto é $\alpha = - \log_p \norm{p}$, então concluímos que $\norm{x} = \norm{x}_p^\alpha$.
\end{proof}

Recordemo-nos de que um anel, ou corpo, com um valor absoluto um anel, ou corpo, é chamado \emph{normado}.

\begin{cor*}
  Vale
  \begin{align}
      & \{  \text{ completamentos (normados) do corpo $\Q$ } \} \\
    = & \{ \R \} \cup \{ \text{ todos os }\Q_p \text{ para $p$ um número primo } \}.
  \end{align}
\end{cor*}
\begin{proof}
  Segue da definição do completamento de um espaço métrico.
\end{proof}

% ex: set spelllang=pt:

